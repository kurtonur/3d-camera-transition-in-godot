extends Camera

export(NodePath) var camera1 = "."
export(NodePath) var camera2 = "."

export(float,0,100,0.1) var duration = 1.0;
export(int, "TRANS_LINEAR" , "TRANS_SINE", "TRANS_QUINT","TRANS_QUART",
		"TRANS_QUAD","TRANS_EXPO","TRANS_ELASTIC","TRANS_CUBIC","TRANS_CIRC",
		"TRANS_BOUNCE","TRANS_BACK") var transitionType = 0;
export(int, "EASE_IN" , "EASE_OUT", "EASE_IN_OUT","EASE_OUT_IN") var easeType = 2;
export(float) var delay = 0.0;

var tweenMove :Tween = Tween.new(); 

func _ready():
	
	camera1 = get_node(camera1) as Camera 
	camera2 = get_node(camera2) as Camera 
	self.transform = camera1.transform
	add_child(tweenMove)
	tweenMove.connect("tween_completed",self,"TweenMoveFinished")
	SetMove()
	pass 

func SetMove():
	
	if self.transform == camera1.transform:
		tweenMove.interpolate_property(
		self,"transform",self.transform,camera2.transform,duration,transitionType,easeType,delay)
	elif self.transform == camera2.transform:
		tweenMove.interpolate_property(
		self,"transform",self.transform,camera1.transform,duration,transitionType,easeType,delay)

func Transfer():
	self.make_current()
	tweenMove.start()
	pass

func TweenMoveFinished(obj, key):
	tweenMove.stop_all()
	SetMove()
	pass
